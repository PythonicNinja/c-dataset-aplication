﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZadanieBazy
{
    public partial class klient : Form
    {
        public System.Windows.Forms.Timer timerSync;
        public System.Windows.Forms.ToolStripStatusLabel status;
        public Boolean sync = false;

        public klient()
        {
            InitializeComponent();
        }

        private void klient_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.ap_klient' table. You can move, or remove it, as needed.
            this.ap_klientTableAdapter.Fill(this.dataSet1.ap_klient);
        }

        private void zapisz_Click(object sender, EventArgs e)
        {
            //aktualizuje bazę o wpisy w 
            ap_klientTableAdapter.Update(dataSet1.ap_klient);
            this.dataGridView1.Refresh();
        }

        private void usun_Click(object sender, EventArgs e)
        {
            //sprawdza czy jest zaznaczony wiersz
            if (dataGridView1.CurrentRow != null)
            {
                dataGridView1.CurrentRow.Selected = true;
                //usuwa zaznaczony wiersz
                dataGridView1.Rows.Remove(dataGridView1.SelectedRows[0]);
            }

        }

        private void klient_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            if (!sync) { 
                DialogResult dr = MessageBox.Show("Czy chcesz zapisać wszystkie zmiany, które wprowadziłeś?", "UWAGA!!!", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    ap_klientTableAdapter.Update(dataSet1.ap_klient);
                }
            }
        }

        private void zapisz_Click_1(object sender, EventArgs e)
        {
            //aktualizuje bazę o wpisy
            ap_klientTableAdapter.Update(dataSet1.ap_klient);
        }


        private void timerSync_Tick(object sender, EventArgs e)
        {
            this.ap_klientTableAdapter.Update(this.dataSet1.ap_klient);
        }

    }
}
