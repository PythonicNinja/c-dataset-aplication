﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZadanieBazy
{

    public partial class wynajem : Form
    {
        public System.Windows.Forms.Timer timerSync;
        public System.Windows.Forms.ToolStripStatusLabel status;
        public Boolean sync = false;

        public wynajem()
        {
            InitializeComponent();
        }

        private void wynajem_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.ap_klient' table. You can move, or remove it, as needed.
            this.ap_klientTableAdapter.Fill(this.dataSet1.ap_klient);
            // TODO: This line of code loads data into the 'dataSet1.ap_wynajem' table. You can move, or remove it, as needed.
            this.ap_wynajemTableAdapter.Fill(this.dataSet1.ap_wynajem);
            // TODO: This line of code loads data into the 'dataSet1.ap_mieszkanie' table. You can move, or remove it, as needed.
            this.ap_mieszkanieTableAdapter.Fill(this.dataSet1.ap_mieszkanie);

            CalendarColumn data_rozp = new CalendarColumn();
            data_rozp.DataPropertyName = "data_rozp";
            data_rozp.HeaderText = "Data rozpoczęcia";
            data_rozp.Name = "data_rozp";
            this.dataGridView1.Columns.Add(data_rozp);

            CalendarColumn data_zak = new CalendarColumn();
            data_zak.DataPropertyName = "data_zak";
            data_zak.HeaderText = "Data zakończenia";
            data_zak.Name = "data_zak";
            this.dataGridView1.Columns.Add(data_zak);

        }

        private void usun_Click(object sender, EventArgs e)
        {
            //sprawdza czy jest zaznaczony wiersz
            if (dataGridView1.CurrentRow != null)
            {
                dataGridView1.CurrentRow.Selected = true;
                //usuwa zaznaczony wiersz
                dataGridView1.Rows.Remove(dataGridView1.SelectedRows[0]);
            }
        }

        private void zapisz_Click(object sender, EventArgs e)
        {
            //aktualizuje bazę o wpisy
            ap_wynajemTableAdapter.Update(dataSet1.ap_wynajem);            
        }

        private void wynajem_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!sync) {
                DialogResult dr = MessageBox.Show("Czy chcesz zapisać wszystkie zmiany, które wprowadziłeś?", "UWAGA!!!", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    ap_wynajemTableAdapter.Update(dataSet1.ap_wynajem);
                }
            }
        }

        private void timerSync_Tick(object sender, EventArgs e)
        {
           this.ap_wynajemTableAdapter.Update(this.dataSet1.ap_wynajem);
        }
    }
}
