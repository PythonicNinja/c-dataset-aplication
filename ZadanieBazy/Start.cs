﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZadanieBazy
{
    public partial class start : Form
    {
        public start()
        {
            InitializeComponent();
        }

        private void klientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            klient k = new klient();
            if (this.tryb.Text == "Tryb połączeniowy")
            {
                k.timerSync.Enabled = true;
                k.sync = true;
                k.status.Text = "Tryb połączeniowy";
            }
            else
            {
                k.status.Text = "Tryb bezpołączeniowy";
            }
            k.MdiParent = this;
            k.Show();
        }

        private void mieszkanieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mieszkanie m = new mieszkanie();
            if (this.tryb.Text == "Tryb połączeniowy")
            {
                m.timerSync.Enabled = true;
                m.sync = true;
                m.status.Text = "Tryb połączeniowy";
            }
            else
            {
                m.status.Text = "Tryb bezpołączeniowy";
            }
            m.MdiParent = this;
            m.Show();
        }

        private void wynajemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            wynajem w = new wynajem();
            if (this.tryb.Text == "Tryb połączeniowy")
            {
                w.timerSync.Enabled = true;
                w.sync = true;
                w.status.Text = "Tryb połączeniowy";
            }
            else
            {
                w.status.Text = "Tryb bezpołączeniowy";
            }
            w.MdiParent = this;
            w.Show();
        }

        private void tryb_Click(object sender, EventArgs e)
        {
            if (this.tryb.Text == "Tryb bezpołączeniowy")
            {
                this.tryb.Text = "Tryb połączeniowy";
                this.tryb.Image = new Bitmap(ZadanieBazy.Properties.Resources.refresh);
            }
            else
            {
                this.tryb.Text = "Tryb bezpołączeniowy";
                this.tryb.Image = new Bitmap(ZadanieBazy.Properties.Resources.non_refresh);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }

    }
}
