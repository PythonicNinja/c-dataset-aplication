﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ZadanieBazy
{
    public partial class mieszkanie : Form
    {
        public System.Windows.Forms.Timer timerSync;
        public System.Windows.Forms.ToolStripStatusLabel status;
        public Boolean sync = false;

        public mieszkanie()
        {
            InitializeComponent();
        }

        private void mieszkanie_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.ap_mieszkanie' table. You can move, or remove it, as needed.
            this.ap_mieszkanieTableAdapter.Fill(this.dataSet1.ap_mieszkanie);
        }

        private void zapisz_Click(object sender, EventArgs e)
        {
            //aktualizuje bazę o wpisy 
            ap_mieszkanieTableAdapter.Update(dataSet1.ap_mieszkanie);
        }

        private void usun_Click(object sender, EventArgs e)
        {
            //sprawdza czy jest zaznaczony wiersz
            if (dataGridView1.CurrentRow != null)
            {
                dataGridView1.CurrentRow.Selected = true;
                //usuwa zaznaczony wiersz
                dataGridView1.Rows.Remove(dataGridView1.SelectedRows[0]);
            }
        }

        private void mieszkanie_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!sync) { 
                DialogResult dr = MessageBox.Show("Czy chcesz zapisać wszystkie zmiany, które wprowadziłeś?", "UWAGA!!!", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    ap_mieszkanieTableAdapter.Update(dataSet1.ap_mieszkanie);
                }
            }
        }

        private void addImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Title = "Open Image";
            dlg.Filter = "jpg files (*.JPG)|*.JPG";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                // Create a new Bitmap object from the picture file on disk,
                // and assign that to the PictureBox.Image property
                pictureBox1.Image = new Bitmap(dlg.FileName);
            }


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ZdjecieMieszkanie z = new ZdjecieMieszkanie();
            z.pictureBox1.Image = this.pictureBox1.Image;
            z.Show();
        }


        private void timerSync_Tick(object sender, EventArgs e)
        {
            //aktualizuje bazę o wpisy 
            ap_mieszkanieTableAdapter.Update(dataSet1.ap_mieszkanie);
        }

    }
}
