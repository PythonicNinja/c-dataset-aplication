USE wnowak
GO

DROP TABLE ap_wynajem;
DROP TABLE ap_klient;
DROP TABLE ap_mieszkanie;
GO


CREATE TABLE ap_klient (
  id int IDENTITY(1, 1) NOT NULL,
  imie varchar(100) NULL,
  nazwisko varchar(100) NULL,
  miasto varchar(100) NOT NULL ,
  kraj varchar(100) NOT NULL,
  ulica varchar(100) NOT NULL,
  nr_domu varchar(100) NOT NULL,
  nr_mieszkania varchar(10) NULL,
  kod_pocztowy varchar(10) NULL,
  telefon varchar(30) NOT NULL,
  pesel varchar(12) NULL,

  PRIMARY KEY CLUSTERED (id)
)
GO


CREATE TABLE ap_mieszkanie (
  id int IDENTITY(1,1) NOT NULL,
  nazwa varchar(100) NULL,
  cena Money NULL,
  zdjecie image NULL,
  miasto varchar(100) NOT NULL ,
  ulica varchar(100) NOT NULL,
  nr_domu varchar(100) NOT NULL,
  nr_mieszkania varchar(10) NULL,

  PRIMARY KEY CLUSTERED (id)
)
GO

CREATE TABLE ap_wynajem (
  id int IDENTITY(1, 1) NOT NULL,
  mieszkanie_fk int NOT NULL REFERENCES ap_mieszkanie(id),
  klient_fk int NOT NULL REFERENCES ap_klient(id),
  data_rozp DATE NOT NULL,
  data_zak DATE NOT NULL,

  zaliczka MONEY NOT NULL,
  kaucja MONEY NOT NULL,
  parking MONEY NOT NULL,
  oplata MONEY NOT NULL,
  
  PRIMARY KEY CLUSTERED (id)
)
GO

INSERT INTO ap_klient VALUES('Wojtek', 'Nowak', 'Sopot', 'PL', 'Wejherowska', '30', NULL, '11-222', '665987443', '92010909237');
INSERT INTO ap_klient VALUES('Michal', 'Michalski', 'Warszawa', 'PL', 'Gdanska', '1', '7a', '71-222', '123456789', '4567897897');
INSERT INTO ap_klient VALUES('Adam', 'Myszke', 'Elbl�g', 'PL', 'Zapolska', '123', '3', '45-782', '456789456', '1235647894');

SELECT * FROM ap_klient;
GO

/*
EOS NIE CHCE :/
DECLARE @img1 VARBINARY(MAX);
SELECT @img1 = BulkColumn 
FROM OPENROWSET(BULK 'blob:https://lh4.googleusercontent.com/-7ovzV1s5uTU/UdnAIDHC5QI/AAAAAAAAAt0/Y-cuRQWNnss/s640/DSC_8227.JPG', SINGLE_BLOB) as imagefile);
*/

INSERT INTO ap_mieszkanie SELECT 'Ludwik', 250, NULL, 'Sopot', 'Ksi���t Pomorskich', '3', '7';
INSERT INTO ap_mieszkanie VALUES('Weranda', 250, NULL, 'Sopot', 'Ksi���t Pomorskich', '3', '1');
INSERT INTO ap_mieszkanie VALUES('Studio', 200, NULL, 'Sopot', 'Ksi���t Pomorskich', '3', '2');
INSERT INTO ap_mieszkanie VALUES('Mocarski', 250, NULL, 'Sopot', 'Ksi���t Pomorskich', '3', '3');

SELECT * FROM ap_mieszkanie;
GO